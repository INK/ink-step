require 'spec_helper'

require 'ink_step/utility_steps/unzip_step'

describe "#perform_step" do
  let(:target_file_name)      { "some_file.zip" }
  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }
  let!(:input_file_path)    { File.join(temp_directory, InkStep::Base::INPUT_FILE_DIRECTORY_NAME) }

  subject               { InkStep::UtilitySteps::UnzipStep.new(base_file_location: temp_directory, position: 1) }

  before do
    create_directory_if_needed(input_file_path)
    FileUtils.cp(target_file, input_file_path)
  end

  describe '#perform_step' do
    specify do
      subject.input_file_manifest = subject.semantically_tagged_manifest
      subject.perform_step

      expect(subject.errors).to match_array([])
    end
  end

  describe '#description' do
    it 'has a description' do
      expect{subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    it 'has a human readable name' do
      expect{subject.class.human_readable_name}.to_not raise_error
    end
  end
end


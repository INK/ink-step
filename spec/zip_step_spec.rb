require 'spec_helper'

require 'ink_step/utility_steps/zip_step'
require 'zip'

describe InkStep::UtilitySteps::ZipStep do
  let(:target_file_name)      { "some_text.html" }
  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }
  let(:input_directory)       { File.join(temp_directory, InkStep::Base::INPUT_FILE_DIRECTORY_NAME) }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
  end

  subject               { InkStep::UtilitySteps::ZipStep.new(base_file_location: temp_directory, position: 1) }

  after do
    FileUtils.rm_rf(Dir.glob("#{temp_directory}/*"))
  end

  describe '#perform_step' do

    specify do
      subject.perform_step

      manifest = subject.send(:semantically_tagged_manifest)

      expect(manifest.size).to eq 1
      first_manifest_item = manifest.first
      expect(first_manifest_item[:path]).to eq "archive.zip"
      expect(first_manifest_item[:tag]).to eq :new
    end
  end

  describe '#description' do
    it 'has a description' do
      expect{subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    it 'has a human readable name' do
      expect{subject.class.human_readable_name}.to_not raise_error
    end
  end
end


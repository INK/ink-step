require 'spec_helper'

require 'ink_step/base'
require 'ink_step/conversion_step'
require 'ostruct'

describe InkStep::Base do
  let(:target_file_name)      { "some_text.html" }
  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }
  let(:base_file_location)   { temp_directory }
  let(:input_file_path)       { File.join(base_file_location, InkStep::Base::INPUT_FILE_DIRECTORY_NAME) }

  let(:subject)               { InkStep::Base.new(base_file_location: base_file_location, position: 1) }
  let(:working_directory)     { subject.send(:working_directory) }

  let(:doc1)                    { 'foo/bar baz/document.html' }
  let(:doc2)                    { 'secret doc.docx' }
  let(:file_list)               { [{path: doc1}, {path: doc2}] }

  before do
    create_directory_if_needed(base_file_location)
    create_directory_if_needed(input_file_path)
    FileUtils.cp(target_file, input_file_path)
  end

  describe '#find_source_file' do
    before do
      allow(subject).to receive(:input_file_manifest).and_return(file_list)
      create_directory_if_needed(working_directory)
      allow(File).to receive(:file?).and_return false
    end

    it 'matches files in subdirectories with file extensions' do
      expect(subject.find_source_file(regex: /\.docx$/)).to eq doc2
    end

    it 'matches files in subdirectories with full file names' do
      expect(subject.find_source_file(regex: /secret doc\.docx/)).to eq doc2
    end

    it 'matches files in subdirectories with part file names with spaces' do
      expect(subject.find_source_file(regex: /secret/)).to eq doc2
    end

    it 'matches files in subdirectories with partial filenames' do
      expect{subject.find_source_file(regex: /doc/)}.to raise_error(/Multiple files/)
    end

    it 'matches subdirectories' do
      expect(subject.find_source_file(regex: /bar/)).to eq doc1
    end

    it 'recognises rubbish' do
      expect{subject.find_source_file(regex: /asdf/)}.to raise_error(/No files/)
    end
  end

  describe '#matching_manifest_files' do
    let!(:subject)               { InkStep::Base.new(base_file_location: base_file_location, position: 1) }

    before do
      allow(subject).to receive(:input_file_manifest).and_return(file_list)
    end

    it 'matches files correctly' do
      expect(subject.matching_manifest_files(/\.docx$/)).to eq [{path: doc2}]
    end

    it 'matches files correctly' do
      expect(subject.matching_manifest_files(/secret doc\.docx/)).to eq [{path: doc2}]
    end

    it 'matches files correctly' do
      expect(subject.matching_manifest_files(/secret/)).to eq [{path: doc2}]
    end

    it 'matches files correctly' do
      expect(subject.matching_manifest_files(/doc/)).to eq [{path: doc1}, {path: doc2}]
    end

    it 'matches files correctly' do
      expect(subject.matching_manifest_files(/bar/)).to eq [{path: doc1}]
    end

    it 'matches files correctly' do
      expect(subject.matching_manifest_files(/asdf/)).to eq []
    end
  end

  describe "#log_as_step" do
    context 'for a message without a path' do
      let(:message) { "A nice message" }

      it 'prints it to the log' do
        expect(subject).to receive(:ap).with(/#{message}/)

        subject.send(:log_as_step, message)
      end

      it 'saves the line in the log array' do
        subject.send(:log_as_step, message)

        expect(subject.process_log.grep(/#{message}/).any?).to be_truthy
      end
    end

    context 'for a message with a path' do
      let(:message) { "#{working_directory}/input_files/input_file.html is a great file!" }

      it 'includes the absolute path in the server logs' do
        expect(subject).to receive(:ap).with(/#{message}/)

        subject.send(:log_as_step, message)
      end
    end
  end

  describe '#execute' do
    before do
      allow(subject).to receive(:raise_and_log_error).and_call_original
      allow(subject).to receive(:ap).and_call_original
    end

    context 'if the input file directory exists' do
      before do
        allow(subject).to receive(:cumulative_file_manifest).and_return({input: [{:path=>"some_text.html", :size=>"129 bytes", :checksum=>"44daf5252708644e58880477fbc69e72"}]})
        subject.execute
      end

      it 'executes successfully' do
        files_in_working_directory = Dir[File.join(working_directory, "*")]
        expect(files_in_working_directory).to eq [File.join(working_directory, target_file_name)]
      end

      it 'writes the output manifest' do
        expect(subject.input_file_manifest).to eq([{:path=>"some_text.html", :size=>"129 bytes", :checksum=>"44daf5252708644e58880477fbc69e72"}])
      end
    end
  end

  describe 'required parameters' do
    before do
      allow(subject).to receive(:input_file_manifest).and_return([{path: target_file_name}])
    end

    context 'when there are no required parameters' do
      specify do
        allow(subject).to receive(:required_parameters).and_return([])
        allow(subject).to receive(:cumulative_file_manifest).and_return({})

        expect{subject.execute(options: {})}.to_not raise_error
        expect{subject.execute(options: {other_param: "some_value"})}.to_not raise_error
      end

    end

    context 'when there are required parameters' do
      context 'and they are provided' do
        context 'with provided params' do
          specify do
            allow(subject).to receive(:required_parameters).and_return([:a_param])
            allow(subject).to receive(:cumulative_file_manifest).and_return({})

            subject.combined_parameters = {a_param: "funny hats"}
            expect{subject.execute}.to_not raise_error
          end
        end

        context 'with default values' do
          specify do
            allow(subject).to receive(:required_parameters).and_return([:a_param])
            allow(subject).to receive(:default_parameter_values).and_return({a_param: "abc"})
            allow(subject).to receive(:cumulative_file_manifest).and_return({})

            expect{subject.execute(options: {})}.to_not raise_error
          end
        end
      end

      context 'and they are not satisfied' do
        it 'with all missing, fails' do
          allow(subject).to receive(:required_parameters).and_return([:a_param, :some_other_param])

          expect{subject.execute}.to raise_error("Missing parameters: a_param, some_other_param")
        end

        it 'with some missing, fails' do
          allow(subject).to receive(:required_parameters).and_return([:a_param, :some_other_param])

          subject.combined_parameters = {a_param: "whatever"}
          expect{subject.execute}.to raise_error("Missing parameters: some_other_param")
        end
      end
    end
  end

  describe '#version' do
    context "if InkStep::Base is being used for some reason" do
      specify do
        expect(subject.version).to eq InkStep::VERSION
      end
    end
    context "for a subclass" do
      context "that does not implement the Version method" do
        it 'throws an error' do
          expect{IncompleteStepClass.new.version}.to raise_error(NotImplementedError)
        end
      end
      context "that does not implement the Version method" do
        specify do
          expect(CorrectlyImplementedStepClass.new.version).to eq "1.3"
        end
      end
    end
  end

  describe '#description' do
    context "if InkStep::Base is being used for some reason" do
      specify do
        expect(InkStep::Base.description).to eq "A basic step that returns the files you provide unchanged"
        expect(InkStep::ConversionStep.description).to eq "A generic step to facilitate conversion"
      end
    end
    context "for a subclass" do
      context "that does not implement the Description method" do
        it 'throws an error' do
          expect{IncompleteStepClass.description}.to raise_error(NotImplementedError)
        end
      end
      context "that does not implement the Description method" do
        specify do
          expect(CorrectlyImplementedStepClass.description).to eq "it's a secret"
        end
      end
    end
  end

  describe '#human_readable_name' do
    context "if InkStep::Base is being used for some reason" do
      specify do
        expect(InkStep::Base.human_readable_name).to eq "Base Step"
      end
    end
    context "for a subclass" do
      context "that does not implement the Human Readable Name method" do
        it 'throws an error' do
          expect{IncompleteStepClass.human_readable_name}.to raise_error(NotImplementedError)
        end
      end
      context "that does not implement the Human Readable Name method" do
        specify do
          expect(CorrectlyImplementedStepClass.human_readable_name).to eq "Correctly Implemented Step"
        end
      end
    end
  end

  describe '#semantically_tagged_manifest' do
    let(:file_manifest) {
      [{
           :path => "a_file.docx",
           :size => "5.3 kB",
           :checksum => "f2447e0904f3f3b84c8b1e710fabfce1"
       },
       {
           :path => "another_file.docx",
           :size => "2.8 kB",
           :checksum => "111017ee94636df62daa98257cc1bfeb"
       }]
    }

    context 'a file has been added' do
      let(:old_file_manifest) {
        [{
             :path => "a_file.docx",
             :size => "5.3 kB",
             :checksum => "f2447e0904f3f3b84c8b1e710fabfce1"
         }]
      }

      let(:expected_result) {
        [{
             path: "a_file.docx",
             size: "5.3 kB",
             checksum: "f2447e0904f3f3b84c8b1e710fabfce1",
             tag: :identical
         },
         {
             path: "another_file.docx",
             size: "2.8 kB",
             checksum: "111017ee94636df62daa98257cc1bfeb",
             tag: :new
         }]
      }

      before do
        allow(subject).to receive(:assemble_manifest).and_return(file_manifest)
        allow(subject).to receive(:input_file_manifest).and_return(old_file_manifest)
      end

      it 'marks that it has been added' do
        expect(subject.semantically_tagged_manifest).to match expected_result
      end
    end

    context 'a file has been modified' do
      let(:old_file_manifest) {
        [{
             :path => "a_file.docx",
             :size => "2.8 kB",
             :checksum => "hahahhahaahahahahahahahahahahah"
         }]
      }

      let(:expected_result) {
        [{
             path: "a_file.docx",
             size: "5.3 kB",
             checksum: "f2447e0904f3f3b84c8b1e710fabfce1",
             tag: :modified
         },
         {
             :path => "another_file.docx",
             :size => "2.8 kB",
             :checksum => "111017ee94636df62daa98257cc1bfeb",
             tag: :new
         }
        ]
      }

      before do
        allow(subject).to receive(:assemble_manifest).and_return(file_manifest)
        allow(subject).to receive(:input_file_manifest).and_return(old_file_manifest)
      end

      it 'marks that it has been added' do
        expect(subject.semantically_tagged_manifest).to eq expected_result
      end
    end
  end

  describe '#assemble_manifest' do
    specify do
      ap subject.send(:assemble_manifest, ({directory: File.join(Dir.pwd, "spec", "fixtures", "files", "unzipped_docx")}))

      expect(subject.send(:assemble_manifest, ({directory: File.join(Dir.pwd, "spec", "fixtures", "files", "unzipped_docx")}))).to match_array unzip_doc_manifest
    end
  end

  describe '#initialize' do

  end
end

def unzip_doc_manifest
  [
      {
      :path => "important_document/docProps/app.xml",
      :size => "512 bytes",
      :checksum => "0a793efef281972e4a8f3fb61db8522c"
      },
      {
      :path => "important_document/docProps/core.xml",
      :size => "731 bytes",
      :checksum => "dd4f13c21f29d34551fce3590d7b48ec"
      },
      {
      :path => "important_document/word/styles.xml",
      :size => "2.7 kB",
      :checksum => "67b7703be0b07964db90914a53c6d073"
      },
      {
      :path => "important_document/word/numbering.xml",
      :size => "4.4 kB",
      :checksum => "81c82d82c04bd50e6ea0c717f6a37735"
      },
      {
      :path => "important_document/word/fontTable.xml",
      :size => "853 bytes",
      :checksum => "d7b72d53b5f090d9fa999e5564c92892"
      },
      {
      :path => "important_document/word/settings.xml",
      :size => "280 bytes",
      :checksum => "d83f9013a8a6bf3647e3812b0d4c427a"
      },
      {
      :path => "important_document/word/document.xml",
      :size => "1.8 kB",
      :checksum => "fc1d0f2dfdb022ac40ff7005f8ad05d0"
      },
      {
      :path => "important_document/word/_rels/document.xml.rels",
      :size => "664 bytes",
      :checksum => "5e0aa4a310402d3df359dc3db5b8eccc"
      },
      {
      :path => "important_document/important_document.docx",
      :size => "5.1 kB",
      :checksum => "2eb95973a1e706f001b08731393d7f90"
      },
      {
      :path => "important_document/process_step_821.log",
      :size => "2.0 kB",
      :checksum => "374e4b3c7bd818e6d09a4f05d3254e49"
      },
      {
      :path => "important_document/docx-html-extract.xsl",
      :size => "23.3 kB",
      :checksum => "ca0df12ef197135a373bdfb11eb86824"
      },
      {
      :path => "important_document/[Content_Types].xml",
      :size => "1.3 kB",
      :checksum => "035b3e3c926458b48f38a01de5e47926"
      },
      {
      :path => "important_document/important_document.html",
      :size => "922 bytes",
      :checksum => "c71a909df2b31c85c479cde408e069e2"
    }
  ]
end

class IncompleteStepClass < InkStep::Base
  def initialize

  end
end

class CorrectlyImplementedStepClass < InkStep::Base
  def initialize

  end

  def self.human_readable_name
    "Correctly Implemented Step"
  end

  def self.description
    "it's a secret"
  end

  def version
    "1.3"
  end
end
require 'spec_helper'

require 'ink_step/validation_steps/arbitrary_validator'
require 'ostruct'

describe "Testing validators" do
  let(:target_file_name)      { "some_text.html" }
  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }
  let!(:input_file_path)    { File.join(temp_directory, InkStep::Base::INPUT_FILE_DIRECTORY_NAME) }

  subject               { InkStep::ValidationSteps::ArbitraryValidator.new(base_file_location: temp_directory, position: 1) }

  before do
    create_directory_if_needed(temp_directory)
    create_directory_if_needed(input_file_path)
    FileUtils.cp(target_file, input_file_path)
  end

  describe '#perform_step' do
    context 'if the step validates OK' do
      it 'passes' do
        subject.combined_parameters = {is_ok?: true}
        subject.perform_step

        expect(subject.errors).to match_array([])
      end
    end

    context 'if the step fails with a warning' do
      it 'fails with a warning' do
        subject.combined_parameters = {is_ok?: false}
        subject.perform_step

        expect(subject.errors).to match_array(["Warning: Not really OK but just letting you know (the pipeline can continue)"])
      end
    end

    context 'if the step fails hard (ie. unrecoverable error)' do
      it 'throws an error' do
        subject.combined_parameters = {is_ok?: false, fail_hard?: true}

        expect{subject.perform_step}.to raise_error(StandardError, "Fail HARD: A BIG PROBLEM! - Stopping pipeline")

        expect(subject.errors).to match_array(["A BIG PROBLEM!"])
      end
    end
  end

  describe '#description' do
    it 'has a description' do
      expect{subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    it 'has a human readable name' do
      expect{subject.class.human_readable_name}.to_not raise_error
    end
  end
end


require 'ink_step/base'
require 'ink_step/mixins/shell_methods'
require 'ink_step/mixins/zip_methods'

module InkStep::UtilitySteps
  class ZipStep < ::InkStep::Base
    include InkStep::Mixins::ZipMethods

    def perform_step
      create_archive
      # cleanup
      delete_other_files
      success!
    end

    def self.description
      "Zips all files in the working directory into an archive"
    end

    def self.human_readable_name
      "Zip Step"
    end

    def version
      InkStep::VERSION
    end

    def required_parameters
      # e.g. [:foo, :bar]
      [:archive_file_name]
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {archive_file_name: "The name of the archive file (including .zip)"}
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {archive_file_name: "archive.zip"}
    end

    protected

    def archive_file_name
      @archive_file_name ||= parameter(:archive_file_name)
    end

    def delete_other_files
      Dir.chdir working_directory
      Dir.glob("**/*").each do |f|
        next if f == archive_file_name
        if File.file?(f)
          log_as_step("Deleting file #{f}")
          File.delete(f)
        elsif File.directory?(f)
          log_as_step("Deleting directory #{f}")
          FileUtils.remove_dir(f)
        end
      end
    end
  end
end
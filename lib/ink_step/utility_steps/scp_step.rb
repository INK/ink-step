require 'ink_step/base'
require 'ink_step/mixins/shell_methods'
require 'ink_step/mixins/zip_methods'

module InkStep::UtilitySteps
  class ScpStep < ::InkStep::Base

    def perform_step
      perform_scp
      success! unless @errors.any?
    end

    def self.description
      "Performs scp command to copy files available to this step to a remote location"
    end

    def self.human_readable_name
      "Secure Copy Step"
    end

    def version
      InkStep::VERSION
    end

    def required_parameters
      # e.g. [:foo, :bar]
      [:user, :remote_server, :destination_location]
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {
          user: "The name of the user on the remote server",
          remote_server: "The address of the remote server",
          destination_location: "The destination location on the remote server"
      }
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {
          destination_location: "~/"
      }
    end

    protected

    def perform_scp
      # scp -r directory user@location:~/destination
      user = parameter(:user)
      location = parameter(:location)
      destination = parameter(:destination)

      command = "scp -r #{Shellwords.escape(working_directory)} #{Shellwords.escape(user)}@#{Shellwords.escape(location)}:#{Shellwords.escape(destination)}"
      perform_command(command: command, error_prefix: "Error", print_out: false)
    end
  end
end
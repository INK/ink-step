require 'ink_step/base'
require 'ink_step/mixins/shell_methods'

module InkStep::UtilitySteps
  class ModifiedFileCollectorStep < ::InkStep::Base

    def perform_step
      discard_identical_files
      success!
    end

    def self.description
      "Keeps all new and modified files from the previous step, discards any unchanged files"
    end

    def self.human_readable_name
      "Modified File Collector"
    end

    def version
      InkStep::VERSION
    end

    protected

    def discard_identical_files
      identical_files = input_file_manifest.select{|file_hash| file_hash[:tag] == :identical}
      log_as_step "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~1"
      log_as_step "Input files"
      log_as_step input_file_manifest
      log_as_step "Identical files"
      log_as_step identical_files
      log_as_step "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~2"
      identical_files.map do |file_hash|
        log_as_step("Removing #{file_hash[:path]}")
        File.delete(File.join(working_directory, file_hash[:path]))
      end
    end
  end
end
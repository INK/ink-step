require 'ink_step/base'
require 'ink_step/mixins/zip_methods'

module InkStep::UtilitySteps
  class UnzipStep < ::InkStep::Base
    include InkStep::Mixins::ZipMethods

    def perform_step
      source_file_path = find_source_file(regex: /\.#{parameter(:zip_file_extension)}$/)
      unzip_archive(File.join(working_directory, source_file_path))

      # cleanup - destroy original archive
      File.delete(File.join(working_directory, source_file_path))

      success!
    end

    def self.description
      "Unzips target archive"
    end

    def self.human_readable_name
      "Unzip Step"
    end

    def version
      InkStep::VERSION
    end

    def required_parameters
      # e.g. [:foo, :bar]
      [:zip_file_extension]
    end

    def accepted_parameters
      {zip_file_extension: "The extension of the archive file to be unzipped"}
    end

    def default_parameter_values
      {zip_file_extension: "zip"}
    end
  end
end
require 'open3'
require 'ink_step/mixins/helper_methods'
require 'ink_step/mixins/shell_methods'
require 'awesome_print'
require 'fileutils'

module InkStep
  class Base
    include Mixins::HelperMethods
    include Mixins::ShellMethods

    attr_accessor :errors, :status_code, :process_step, :started_at, :finished_at,
                  :position, :base_file_location, :successful, :notes, :process_log,
                  :combined_parameters, :input_file_manifest, :output_file_manifest,
                  :cumulative_file_manifest

    INPUT_FILE_DIRECTORY_NAME = "input_files"
    OUTPUT_FILE_DIRECTORY_NAME = "output_files"

    def initialize(base_file_location:, position:)
      @position = position
      @base_file_location = base_file_location
      @successful = nil
      @errors = []
      @notes = []
      @combined_parameters = {}
      @cumulative_file_manifest = {}

      create_directory_if_needed(working_directory)
      get_input_files
    end

    def execute(options: {})
      @combined_parameters ||= options
      @started_at = Time.now
      check_parameters
      self.input_file_manifest = input_manifest_from_cumulative
      log_as_step "Incoming file manifest:"
      log_as_step input_file_manifest
      log_as_step "Using version #{version}"
      log_as_step "Execution parameters:"
      log_as_step @combined_parameters
      perform_step
    rescue => e
      raise_and_log_error(e)
      @successful = false
    ensure
      self.output_file_manifest = semantically_tagged_manifest
      log_as_step "Wrote output file manifest:"
      log_as_step output_file_manifest
      @finished_at = Time.now
      log_as_step "Finished at #{@finished_at.utc.strftime("%Y-%m-%d %H:%M:%S UTC")}"
      log_as_step "Errors: #{@errors}"
    end

    # A Basic Step on its own doesn't change the file at all
    def perform_step
      # whatever the step does would normally go here
    end

    def raise_and_log_error(error)
      message = error.respond_to?(:message) ? error.message : error
      @errors << message
      ap message
      ap error.backtrace if error.respond_to?(:backtrace)
      raise error
    end

    def check_parameters
      ap "checking parameters: required params"
      ap required_parameters
      ap "Supplied parameters"
      ap @combined_parameters
      missing_parameters = required_parameters.map(&:to_s) - @combined_parameters.keys.map(&:to_s) - default_parameter_values.keys.map(&:to_s)
      raise ArgumentError.new("Missing parameters: #{missing_parameters.join(", ")}") if missing_parameters.any?
    end

    # Override this method with parameters which must be provided.
    # If you've set a param as required but you have also specified a default,
    # INK will just use that default value.
    # It's good to put it here anyway since if you remove any default values,
    # INK will still be able to catch missing ones. :)
    def self.required_parameters
      # e.g. [:foo, :bar]
      []
    end

    # Override this method with any parameters the step uses, and describe each.
    def self.accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {}
    end

    # Override this method with default values
    def self.default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {}
    end

    # For retrieving parameters
    def parameter(symbol)
      @combined_parameters[symbol.to_s] || @combined_parameters[symbol] || default_parameter_values[symbol] || default_parameter_values[symbol.to_s]
    end

    def required_parameters
      self.class.required_parameters
    end

    def accepted_parameters
      self.class.accepted_parameters
    end

    def default_parameter_values
      self.class.default_parameter_values
    end

    def version
      return InkStep::VERSION if self.class.name == "InkStep::Base"
      raise NotImplementedError.new("You need to provide a Version method in your step (#{self.class.name}) that points to your gem version (see CokoDemoSteps for an example)")
    end

    def self.description
      return "A basic step that returns the files you provide unchanged" if self.name == "InkStep::Base"
      raise NotImplementedError.new("You need to provide a Description method in your step (#{self.class.name}) that returns a string - a brief summary of what your step does (see CokoDemoSteps for an example)")
    end

    def self.human_readable_name
      return "Base Step" if self.name == "InkStep::Base"
      raise NotImplementedError.new("You need to provide a Human Readable Name method in your step (#{self.class.name}) that returns a string - a concise name for your step (see CokoDemoSteps for an example)")
    end

    def working_directory
      @working_directory ||= File.join(base_file_location, position.to_s)
    end

    def process_log
      @process_log ||= []
    end

    def get_input_files
      if first_step_in_chain?
        source = File.join(base_file_location, INPUT_FILE_DIRECTORY_NAME)
      else
        source = File.join(base_file_location, "#{position-1}")
      end
      log_as_step "Getting files from directory: #{source}..."
      raise "Cannot find source directory #{source}" unless File.directory?(source)
      raise "Source directory #{source} is empty" if Dir["#{source}/*"].empty?
      copy_files_recursively(source, working_directory)
      log_as_step "Files retrieved successfully from #{source}"
    end

    def semantically_tagged_manifest
      file_manifest_at_end = assemble_manifest(directory: working_directory)
      file_manifest_at_end.each do |file_hash|
        start_file_hash = find_start_file(file_hash)
        if start_file_hash.nil?
          file_hash[:tag] = :new
        elsif file_hash[:checksum] == start_file_hash[:checksum]
          file_hash[:tag] = :identical
        else
          file_hash[:tag] = :modified
        end
      end
    end

    protected

    def log_as_step(message)
      message_with_metadata = "#{Time.now.utc.strftime("%y-%m-%d %H:%M:%S")} [#{self.class.name}] - #{message}"
      ap message_with_metadata
      process_log << message_with_metadata
    end

    def success!
      @successful = true
    end

    private

    def input_manifest_from_cumulative
      if first_step_in_chain?
        cumulative_file_manifest[:input]
      else
        cumulative_file_manifest[position-1]
      end
    end

    def first_step_in_chain?
      position == 1
    end

    def assemble_manifest(directory: working_directory)
      files = recursive_file_list(directory: directory)
      log_as_step recursive_file_list(directory: directory)
      files.inject([]) do |result, file_relative_path|
        file_info_hash = {}
        file_info_hash[:path] = file_relative_path
        absolute_file_path = File.join(directory, file_relative_path)
        file_info_hash[:size] = file_size_for_humans(absolute_file_path)
        file_info_hash[:checksum] = Digest::MD5.hexdigest(File.read(absolute_file_path))
        result << file_info_hash
        result
      end
    end

    def recursive_file_list(directory: working_directory)
      Dir.chdir(directory) do
        files = Dir["**/*.*"]
        files_only = []

        files.each do |file|
          full_path = File.join(directory, file)
          next unless File.file?(full_path)
          ap "Adding #{full_path}"
          if "#{File::SEPARATOR}#{file}".match(/#{directory}/)
            relative_path = "#{File::SEPARATOR}#{file}".gsub(/#{directory}\//, "")
          else
            relative_path = file
          end
          files_only << relative_path
        end
        files_only
      end
    end

    def file_size_for_humans(path)
      file_size_in_bytes = File.size(path)
      if file_size_in_bytes > 299000
        "#{(file_size_in_bytes / 1000000.0).round(1)} MB"
      elsif file_size_in_bytes > 1000
        "#{(file_size_in_bytes / 1000.0).round(1)} kB"
      else
        "#{file_size_in_bytes} bytes"
      end
    end

    def find_start_file(current_file_hash)
      return unless input_file_manifest
      input_file_manifest.collect{|f| return f if f[:path] == current_file_hash[:path]}.first
    end
  end
end
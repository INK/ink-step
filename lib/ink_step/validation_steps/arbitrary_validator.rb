require 'ink_step/validation_step'

module InkStep
  module ValidationSteps
    class ArbitraryValidator < InkStep::ValidationStep

      def perform_step
        super
        is_ok = @combined_parameters[:is_ok?]
        fail_hard = @combined_parameters[:fail_hard?]

        if is_ok
          "Looks great! A-OK!"
        elsif fail_hard
          error_text = "A BIG PROBLEM!"
          @errors << error_text
          raise StandardError.new("Fail HARD: #{error_text} - Stopping pipeline")
        else # fail, but not hard
          @errors << "Warning: Not really OK but just letting you know (the pipeline can continue)"
        end
      end

      def version
        InkStep::VERSION
      end

      def self.description
        "A demo validator that returns the result you tell it to."
      end

      def self.human_readable_name
        "Arbitrary Validator"
      end
    end
  end
end
require 'securerandom'
require 'base64'

module InkStep
  module Mixins
    module HelperMethods

      def snake_case_to_camel_case
        class_name.split('_').collect(&:capitalize).join
      end

      def class_name
        self.class.name.split('::').last
      end

      def random_alphanumeric_string
        Base64.encode64(SecureRandom.uuid)[0..10]
      end
    end
  end
end
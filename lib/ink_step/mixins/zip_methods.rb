require 'zip'

module InkStep
  module Mixins
    module ZipMethods
      def unzip_archive(input_file_path)
        Zip::File.open(input_file_path) do |zip_file|
          @docx_archive_name = File.basename(zip_file.name,File.extname(zip_file.name))
          log_as_step "Unzipping #{@docx_archive_name} as #{zip_file.name}"

          zip_file.each do |f|
            path = File.join(unzip_directory, f.name)
            log_as_step "Unzipping #{f} to #{path}"
            FileUtils.mkdir_p(File.dirname(path)) unless File.exists?(File.dirname(path))
            f.extract(path)
          end
        end
      end

      def create_archive
        log_as_step "Creating archive #{archive_file_name}..."

        Zip::File.open(archive_file_name, Zip::File::CREATE) do |zipfile|
          Dir.chdir working_directory
          Dir.glob("**/*").reject {|fn| File.directory?(fn) }.each do |file|
            file_relative_path = file.gsub(working_directory + '/', '')
            log_as_step "Adding #{file_relative_path}"
            zipfile.add(file_relative_path, File.join(working_directory, file))
          end
        end
      end

      def document_xml_path
        @document_xml_path ||= File.join(unzip_directory, "word", "document.xml")
      end

      def unzip_directory
        File.join(working_directory, @docx_archive_name)
      end
    end
  end
end
require 'open3'

module InkStep
  module Mixins
    module ShellMethods

      def source_file_relative_path_by_extension(extensions)
        Dir.chdir(working_directory) do
          target_files = []
          extensions.each do |extension|
            target_files = Dir.glob(File.join("**", "*.#{extension}")) if target_files.empty?
          end
          return target_files.first if target_files.count == 1
          raise "Multiple files found matching extensions #{extensions} - please specify a single filename using the regex" if target_files.count > 1
          raise "No files matching #{extensions} found"
        end
      end

      def source_file_relative_path_by_input_filename(input_file_path:)
        check_input_file_path(input_file_path)
        input_file_path
      end

      def delete_file(file)
        File.delete(file)
      end

      def check_input_file_path(input_file_path)
        raise "Cannot find source file #{input_file_path}" if input_file_path && !input_file_path.empty? && !File.exists?(input_file_path)
      end

      def find_source_file(regex:)
        regexes = [regex].flatten
        regexes.each do |regex|
          matching_files = matching_manifest_files(regex)
          if matching_files.empty?
            next
          elsif matching_files.count == 1
            return matching_files.first[:path]
          else
            raise "Multiple files found matching regex #{regexes.map(&:to_s).join(", ")} - please be more specific (files found: #{matching_files.join(",")})."
          end
        end
        raise "No files matching #{regexes.join(",")} found"
      end

      def matching_manifest_files(regex)
        self.input_file_manifest = semantically_tagged_manifest if input_file_manifest.nil?
        input_file_manifest.select do |file_hash|
          file_hash[:path].match(regex)
        end
      end

      def create_directory_if_needed(path)
        FileUtils.mkdir_p(path) unless File.directory?(path)
      end

      def copy_files_recursively(source_directory, destination_directory)
        log_as_step "Copying files from #{source_directory} to #{destination_directory}"
        FileUtils.cp_r "#{source_directory}/.", destination_directory, verbose: true
      end

      def perform_command(command:, error_prefix: "Error", print_out: false)
        log_as_step "running #{command}"

        Open3.popen3(command) do |stdin, stdout, stderr, wait_thr|
          exit_status = wait_thr.value
          @success = exit_status.success?
          err = stderr.read
          @notes << stdout.read
          if @success
            @errors << err if print_out
          else
            log_as_step "#{error_prefix}: #{err}"
            @errors << err
          end
        end
      end
    end
  end
end
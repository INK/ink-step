require 'ink_step/base'

module InkStep
  class ConversionStep < Base

    def self.description
      "A generic step to facilitate conversion"
    end

    def self.human_readable_name
      "Conversion Step"
    end
  end
end
require 'ink_step/base'

module InkStep
  class ValidationStep < Base

    def self.description
      "A base validator."
    end
  end

end
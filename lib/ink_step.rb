module InkStep
  if defined?(Rails)
    require 'ink_step/engine'
  else
    require 'ink_step/base'
    require 'ink_step/conversion_step'
    require 'ink_step/validation_step'
    require 'ink_step/validation_steps/arbitrary_validator'
    require 'ink_step/conversion_error'
  end
end
# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'ink_step/version'

Gem::Specification.new do |spec|
  spec.name           = 'ink_step'
  spec.version        = InkStep::VERSION
  spec.date           = '2016-10-05'
  spec.summary        = "A very basic step upon which to base your own"
  spec.description    = "A basic step for extending to suit your own needs within the INK pipelining framework"
  spec.authors        = ["Charlie Ablett"]
  spec.email          = 'charlie@coko.foundation'
  spec.homepage       = 'https://gitlab.coko.foundation/INK/ink-step'
  spec.license        = 'MIT'

  spec.executables    = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files     = spec.files.grep(%r{^(spec|features)/})
  spec.require_paths  = %w(lib)

  spec.add_dependency "awesome_print"
  spec.add_dependency "rubyzip"
  spec.add_dependency 'httparty'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rake"

  spec.required_ruby_version = '~> 2.2'
  spec.files         = Dir.glob("{lib}/**/*") + %w(./README.md)
end